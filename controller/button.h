#ifndef BUTTON_H
#define BUTTON_H

enum BtnState
{
	NOT_PRESSED, PRESS_PENDING, SINGLE_PRESS, HOLD_DOWN, HOLD_DOWN_WAIT_RELEASE
};

struct BtnInfo
{
	BtnState state;
	uint32_t time;
};

#endif