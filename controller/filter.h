/**
* Defines a simple sensor filter
*
*/
#ifndef FILTER_H_
#define FILTER_H_

const uint8_t FILTER_BUFFER_SIZE = 32;
static uint8_t _tmpbuffer[FILTER_BUFFER_SIZE*4];

template<class T> 
class SensorFilter
{
	private:
		
		T _buffer[FILTER_BUFFER_SIZE];
		uint8_t bufferptr;
		
	public:
		SensorFilter()
		{
			bufferptr = 0;
			memset(_buffer,0,sizeof(T)*FILTER_BUFFER_SIZE);
		}
		
		void addInput(T i)
		{
			_buffer[bufferptr]  =i;
			bufferptr++;
			bufferptr%=FILTER_BUFFER_SIZE;
		}
		
		T getValue()
		{
			//Take the mode of the buffer
			T* a = (T*)_tmpbuffer;
			memcpy(a,_buffer,sizeof(T)*FILTER_BUFFER_SIZE);
			int32_t tmp = 0;
			uint16_t totalweight = 0;
			uint8_t ptr = (bufferptr);
			for(uint8_t i = 0; i < FILTER_BUFFER_SIZE; i++)
			{
				bufferptr--;
				if(bufferptr>FILTER_BUFFER_SIZE)//happens when it underflows
					bufferptr+=FILTER_BUFFER_SIZE; //should make it overflow, making everything happy again
				int32_t weight = (FILTER_BUFFER_SIZE-i)/4+1;
				tmp+=a[bufferptr]*weight;
				totalweight+=weight;
			}
			T med = tmp/(totalweight);
			return med;
		}
};

#endif