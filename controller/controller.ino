#include <stdtypes.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include "filter.h"
#include "button.h"
// LCD Control
const uint8_t TxPin=1;
const uint8_t LtBtnInPin = 5;
const uint8_t RtBtnInPin = 6;
const uint8_t Photo_resist_pin = A8;
const uint8_t LCD_WIDTH = 16;
const uint16_t ButtonPressDownLength = 1000; //long press value

//System control
const uint8_t LightCtrlPin = 13;
const uint8_t USBCtrlPin = 11;
const uint8_t SYSCtrlPin = 12;

//System sensor
const uint8_t WheelFreqPin = 2;
const uint8_t BattChargeStatusPin = 3;
const uint8_t BattVoltageStatusPin = A5;
const uint8_t CurrentInputPin = A4;
const uint8_t CurrentOutputPin = A3;

//Wheel sensor vars
volatile uint16_t wheelfreq_count = 0;
uint32_t wheelfreq_lasttime = 0;
const uint32_t wheelfreq_conv = 64; 
const uint32_t wheelfreq_circum = 103;//76;
//Battery calibration
const uint8_t bat_cali_size = 6;
// 0,20,40,60, 80, 100
//Taken from http://rc.runryder.com/helicopter/t550875p1/
const uint16_t bat_cali[bat_cali_size]={737,778,799,820,840, 860};

//Current calibration
uint8_t calibrate = 1;
const int16_t CurrentOutputLow = 160;
const int16_t CurrentOutputHigh = 640;

const int16_t CurrentInput_Cali_Offset = 165;
const int16_t CurrentInput_Cali_Scale = 45; //in 16ma per raw input
const int8_t CurrentInput_Voltage = 4;

int16_t CurrentOutput_Cali_Offset = 110;
int16_t CurrentOutput_Cali_Scale = 29; //in 16ma per raw input
const uint8_t CurrentOutput_Voltage = 5;

//Sensor filtering
SensorFilter<uint8_t> filter_bat;
SensorFilter<int16_t> filter_currentIn;
SensorFilter<int16_t> filter_currentOut;
SensorFilter<int16_t> filter_photo;

uint32_t CurrentOutput_LastTime = 0;
uint32_t CurrentInput_LastTime = 0;
uint32_t BatteryCharge_LastTime = 0;

//Power control
uint32_t PowerCoolOffTime = 4000;
uint8_t USB_Regvalue = 255;

//Screen Control
uint8_t screen = 0;

//Light Control
uint8_t light_mode = 0;
uint8_t light_on = 0;

uint8_t backlight_mode = 0;
uint8_t backlight_on = 0;

BtnInfo Button_Left = {NOT_PRESSED,0};
BtnInfo Button_Right = {NOT_PRESSED,0};

//Screen Info
uint8_t bat_capacity;
uint32_t cur_speed = 0;
int16_t charge_rate = 0;
int16_t discharge_rate = 0;

uint32_t inactive_time = 0;//to chec  if the device should goto sleep
uint8_t force_sleep = 0;

uint32_t backlight_timeout = 0;
uint32_t light_timeout = 0;

// Variables for Easter Egg
uint8_t egg_state = 0;

//Extrenal interrupt handler (used for wheel count)
ISR(INT1_vect)
{
	if(!digitalRead(2))
	{
		wheelfreq_count++;
	}
}

void setup() 
{
	//initiate power control
	pinMode(SYSCtrlPin,OUTPUT);
	pinMode(USBCtrlPin,OUTPUT);
	pinMode(LightCtrlPin,OUTPUT);	
	
	//Everything off except system (lcd+sensors)
	digitalWrite(SYSCtrlPin,HIGH);
	digitalWrite(USBCtrlPin,LOW);
	digitalWrite(LightCtrlPin,LOW);
	
	//Enable external interrupt 1 (rising edge)
	EIMSK |= (INTF1<<1);
	EICRA |= (ISC11<<1)|(ISC10<<1);
	wheelfreq_lasttime = millis();
	
	//initiate LCD for output
	pinMode(LtBtnInPin, INPUT);
	pinMode(RtBtnInPin, INPUT);
	pinMode(Photo_resist_pin, INPUT);
	
	digitalWrite(TxPin, HIGH);

	Serial1.begin(2400);
	delay(50);
	Serial1.write(12);                 // Clear             
	delay(5);                          // Required delay  
	Serial1.write(22);                 // Set curser to off   
  
	// Define custom character 2 (Blocks for Graph)
	Serial1.write(250);
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
	Serial1.write(31); // [11111]
  
	screen = 0; // Start at screen 1
	//Enable interrupts
	sei();
	//Initiate variable starting values
	light_on = 0;
	backlight_on = 0;
	inactive_time = millis();
	PowerCoolOffTime = millis()+2000;
	light_timeout = 0;
	backlight_timeout = 0;
	egg_state = 0;
}
void loop() 
{
	handle_sleep();
	handlePowerControl();
	//Reading in sensor values regardless, as the filter routine assumes values being taken in at regular intervals
	cur_speed = get_speed(); // Get the current speed in mph
	charge_rate = get_charge(); // Get the current charge rate in mA
	discharge_rate = get_discharge(); // Get the current discharge rate in mA
	bat_capacity = get_capacity(); // Get the current battery capacity in mA
	
	handleButton(LtBtnInPin,Button_Left); // Check Left Button pressed state
	handleButton(RtBtnInPin,Button_Right); // Check Right Button pressed state
	
	int Backlight_switch = analogRead(Photo_resist_pin); // Read the input from the photoresistor
	filter_photo.addInput(Backlight_switch);
	Backlight_switch = filter_photo.getValue();
	if(Backlight_switch<600) // Below 600 is the threashold for darkness
	{
		if(light_mode==2) // Light Mode "Auto"
		{
			// Check to see if it's passed the threashold for at least 10000 milliseconds
			if(!light_on&&light_timeout<millis())
			{
				light_on = 1;
			}
			else if(light_on)
			{
				light_timeout = millis()+10000;
			}
		}
		else
		{
			light_timeout = millis()+10000;
		}
		if(backlight_mode==2) // Backlight Mode "Auto"
		{
			// Check to see if it's passed the threashold for at least 10000 milliseconds
			if(!backlight_on&&backlight_timeout<millis())
			{
				backlight_on = 1;
			}
			else if(backlight_on)
			{
				backlight_timeout = millis()+10000;
			}
		}
		else
		{
			backlight_timeout = millis()+10000;
		}
	}
	else
	{
		if(light_mode==2) // Light Mode "Auto"
		{
			// Check to see if it's passed the threashold for at least 10000 milliseconds
			if(light_on&&light_timeout<millis())
			{
				light_on = 0;
			}
			else if(!light_on)
			{
				light_timeout = millis()+10000;
			}
		}
		else
		{
			light_timeout = millis()+10000;
		}
		if(backlight_mode==2) // Backlight mode "Auto"
		{
			// Check to see if it's passed the threashold for at least 10000 milliseconds
			if(backlight_on&&backlight_timeout<millis())
			{
				backlight_on = 0;
			}
			else if(!backlight_on)
			{
				backlight_timeout = millis()+10000;
			}
		}
		else
		{
			backlight_timeout = millis()+10000;
		}			
	}
	
	if(light_mode!=2)
		light_on = light_mode;
	
	if(backlight_mode!=2)
		backlight_on = backlight_mode;
		
	if(backlight_on)
	{
		Serial1.write(17); // Turns backlight on
	}
	else
	{
		Serial1.write(18); // Turns backlight off
	}
	
	// Left -> Right -> Left -> Right = Easter Egg
	if(Button_Left.state==NOT_PRESSED)
	{
		if(Button_Right.state==SINGLE_PRESS)
		{
			Serial1.write(211);
		  
			Serial1.write(220);
			// Easter Egg
			if(egg_state==0)
			{
				egg_state = 0;
			}
			else if(egg_state==1)
			{
				egg_state++;
				egg_state%=4;
			}
			else if(egg_state==2)
			{
				egg_state = 0;
			}
			else if(egg_state==3)
			{
				egg_state = 0;
				still_alive();
				delay(10000);
				still_alive_chorus();
			}
			// Increment screen, and reset button state
			screen++;
			screen%=4;
			Button_Right.state = NOT_PRESSED;
			Serial1.write(232);
		}
		else if(Button_Right.state==HOLD_DOWN)
		{
			Serial1.write(211);
		  
			Serial1.write(220);
			Serial1.write(220);
			// Reset easter egg state
			egg_state = 0;
			// Increment screen, and reset button state
			Button_Right.state = HOLD_DOWN_WAIT_RELEASE;			
			backlight_mode++;
			backlight_mode%=3;
			Serial1.write(232);
			Serial1.write(232);			
			screen=2;//change it to the light screen
		}
	}
	else if(Button_Right.state==NOT_PRESSED)
	{
		if(Button_Left.state==SINGLE_PRESS)
		{
			Serial1.write(211);
		  
			Serial1.write(220);  	
			// Easter Egg
			if(egg_state==0)
			{
				egg_state++;
				egg_state%=4;
			}
			else if(egg_state==1)
			{
				egg_state = 0;
			}
			else if(egg_state==2)
			{
				egg_state++;
				egg_state%=4;
			}
			else if(egg_state==3)
			{
				egg_state = 0;
			}
			// Increment screen, and reset button state
			screen--;
			if(screen==0xFF)
				screen = 3;
			Button_Left.state = NOT_PRESSED;
			Serial1.write(232);

		}
		else if(Button_Left.state==HOLD_DOWN)
		{
			Serial1.write(211);
		  
			Serial1.write(220);
			Serial1.write(220);
			// Reset Easter Egg State
			egg_state = 0;
			// Increment screen, and reset button state
			Button_Left.state=HOLD_DOWN_WAIT_RELEASE;
			light_mode++;
			light_mode%=3;
			Serial1.write(232);
			Serial1.write(232);
			screen=2;//change it to the light screen
		}
	}
	else if(Button_Right.state==HOLD_DOWN || Button_Left.state==HOLD_DOWN)
	{
		egg_state = 0;
		Serial1.write(211);
	  
		Serial1.write(220);
		Serial1.write(220);
		Serial1.write(220);
		Button_Left.state=HOLD_DOWN_WAIT_RELEASE;
		Button_Right.state=HOLD_DOWN_WAIT_RELEASE;
		force_sleep = 1;
	}

	String str_tmp="";
	// Display Stats based on screen
	// Screen 1: Speed
	// Screen 2: Battery Charge
	// Screen 3: USB/Light Status
	// Screen 4: Battery Discharge rate, Dynamo Output
	uint8_t x = 0;
	switch(screen)
	{
	  case 0: // Speedometer Screen		
		Serial1.write(128);                 // Clear             
		Serial1.write("Current Speed:  ");
		str_tmp+=cur_speed;
		str_tmp+=" MPH";
		padLCD(LCD_WIDTH-str_tmp.length());
		Serial1.print(str_tmp);
		break;
	  case 1: // Battery Capacity Screen
		Serial1.write(128);                 // Clear          
		str_tmp = "Bat. Cap: ";
		str_tmp+=bat_capacity;
		str_tmp+="%";
		Serial1.print(str_tmp);
		padLCD(LCD_WIDTH-str_tmp.length());

		// 6.25% per block
		
		for (; x <= (bat_capacity*4/25); x++)
		{
		  Serial1.write(2);
		}
		padLCD(LCD_WIDTH-x);
		
		break;
	  case 2: // Light and Backlight Status Screen
		Serial1.write(128);      
		str_tmp = "Light: ";
		
		if(light_mode == 1)
		{
			str_tmp += "On";
		}
		else if(light_mode==0)
		{
			str_tmp+="Off";
		}
		else
		{
			str_tmp+="Auto";
		}
		Serial1.print(str_tmp);
		padLCD(LCD_WIDTH-str_tmp.length());
		     
		str_tmp = "Backlight: ";
		
		if(backlight_mode == 1)
		{
			str_tmp += "On";
		}
		else if(backlight_mode==0)
		{
			str_tmp+="Off";
		}
		else
		{
			str_tmp+="Auto";
		}
		Serial1.print(str_tmp);
		padLCD(LCD_WIDTH-str_tmp.length());
		break;
	  case 3: // Charge in, Charge out, and Charge battery Screen
		Serial1.write(128);                 // Clear             
		str_tmp = "Chrg. In:  ";
		printWattage(charge_rate,str_tmp);
		Serial1.print(str_tmp);
		padLCD(LCD_WIDTH-str_tmp.length());
		if((millis()/1500)%2)
		{
			str_tmp = "Chrg. Out: ";
			printWattage(discharge_rate,str_tmp);
		}
		else
		{
			str_tmp = "Chrg. Bat: ";
			printWattage(get_batteryCharge(),str_tmp);
		}
		Serial1.print(str_tmp);
		padLCD(LCD_WIDTH-str_tmp.length());
		break;
	  default: // Something broke?!
		Serial1.write(128);                 // Clear             
		Serial1.write("|----Whoops----|");
		Serial1.write(13);
		Serial1.write("Something  Broke");
		break;
	}
	Serial1.write(22);                 // Set curser to off   
	delay(20);
}

void handle_sleep()
{
	if(cur_speed!=0)
		inactive_time = millis(); //reset it
	if((millis()-inactive_time>5*60*1000) || (cur_speed==0&&force_sleep))//5min
	{
		//goto sleep
		//Turn everything off!!!!!
		digitalWrite(USBCtrlPin,0);
		analogWrite(LightCtrlPin,0);
		digitalWrite(SYSCtrlPin,0);
		Serial1.end();
		delay(100);//Needed to clear queued communication The UART won't release the port until then
		pinMode(0,INPUT);
		digitalWrite(0,HIGH);
		pinMode(TxPin,INPUT); //puts pin in high-impdeance
		digitalWrite(TxPin,HIGH);//Important, or current will flow from LCD to Tx
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);   
	
		sleep_enable();
		sleep_mode(); //GOODNIGHT!!!!!!!!!!
		sleep_disable();
		
		setup();//Quick hack should remove and replace with proper solution
	}
	force_sleep = 0;
}

void printWattage(int16_t v, String& str)
{
	int16_t watt = v/1000;
	int16_t dec = abs(v%1000)/100;
	str+=watt;
	str+=".";
	str+=dec;
	str+="W";
}

inline void padLCD(uint8_t pad)
{
	for(uint8_t i = 0; i < pad;i++)
		Serial1.write(' ');
}

void handleButton(uint8_t pin, BtnInfo& btn)
{
	if(!digitalRead(pin))
	{
		if(btn.state!=HOLD_DOWN&&btn.state!=HOLD_DOWN_WAIT_RELEASE)
		{
			btn.state = PRESS_PENDING;
			if(millis()-btn.time>ButtonPressDownLength)
			{
				btn.state = HOLD_DOWN;
			}
		}			
	}
	else
	{
		if(btn.state == PRESS_PENDING)
			btn.state = SINGLE_PRESS;
		else if(btn.state==HOLD_DOWN_WAIT_RELEASE)
			btn.state = NOT_PRESSED;
		btn.time = millis();		
	}
}

void handlePowerControl()
{
	int16_t power = get_powerOutput();
	if(power>4500) //At 4.5W mark enter power regulation mode
	{
		int16_t diff = power-4500;
		USB_Regvalue = 0xFF-diff/8;
	}
	else
		USB_Regvalue = 0xFF;
	if(power<6000&&PowerCoolOffTime<millis())
	{
		analogWrite(USBCtrlPin,USB_Regvalue);
		analogWrite(LightCtrlPin,light_on*150);
	}
	else if(power>6000)
	{
		if(USB_Regvalue)
		{
			USB_Regvalue = 0;
			analogWrite(USBCtrlPin,0);
		}
		else
			analogWrite(LightCtrlPin,0);
		PowerCoolOffTime = millis()+2000; //2 sec cool off
	}	
}

uint32_t get_speed() // Calculates the current speed of the bike and returns it in mph
{
	static uint32_t speed = 0;
	uint32_t ctime = millis();
	if(ctime-wheelfreq_lasttime>1000)
	{
		cli();
		uint32_t tmpspeed = wheelfreq_count;;
		wheelfreq_count = 0;
		sei();
		uint32_t dtime = ctime-wheelfreq_lasttime;
		wheelfreq_lasttime = ctime;

		//do it piecemeal to advoid overflow and lost of precission
		tmpspeed*=625;
		tmpspeed/=11;
		tmpspeed*=wheelfreq_circum;
		tmpspeed/=dtime;
		tmpspeed/=wheelfreq_conv;
		
		speed = tmpspeed;		
	}
	return speed;
}
/**
 * Get output power in mW
 */
int16_t get_powerOutput()
{
	if(millis()-CurrentOutput_LastTime>40)
	{
		CurrentOutput_LastTime = millis(); 
		int32_t rawv = analogRead(CurrentOutputPin);
		int32_t correction = ((767L*rawv)/1000)+1118;
		int32_t tcurrent = (rawv-((CurrentOutput_Cali_Offset*correction)/1024));
		tcurrent*=(1024*CurrentOutput_Cali_Scale)/correction;
		tcurrent/=8;
		int32_t tpower = CurrentOutput_Voltage*tcurrent;
		filter_currentOut.addInput(tpower);
	}	
	int16_t tmp= filter_currentOut.getValue();
	if(tmp<0)
		return 0;
	return tmp;
}

int16_t get_powerInput()
{
	if(millis()-CurrentInput_LastTime>40)
	{
		CurrentInput_LastTime = millis(); 
		int16_t rawv = analogRead(CurrentInputPin);
		int16_t tcurrent = (rawv-CurrentInput_Cali_Offset)/8;
		tcurrent*=CurrentInput_Cali_Scale;
		tcurrent/=2;
		int16_t tpower = CurrentInput_Voltage*tcurrent;
		filter_currentIn.addInput(tpower);
	}
	int16_t tmp = filter_currentIn.getValue();
	if(tmp<0)
		return 0;
	return tmp;
}

inline int16_t get_batteryCharge()
{
	return (int32_t)get_powerOutput()*11L/10L-(int32_t)get_powerInput();
}

inline int16_t get_discharge() // returns the power that is being drained from the battery
{
	return get_powerOutput();
}

inline int16_t get_charge() // returns the power the dynamo is offering
{
	return get_powerInput();
}

uint8_t get_capacity() // retrieves the current capacity of the battery and returns a percentage
{
	if(millis()-BatteryCharge_LastTime>100)
	{
		BatteryCharge_LastTime = millis();
		uint16_t tmp = analogRead(BattVoltageStatusPin);
		uint8_t debug=tmp/4;
		int8_t i;
		for(i= bat_cali_size-1; i >= 0; i--)
		{
			if(tmp>=bat_cali[i])
			{
				break;
			}
		}
		uint8_t chg;
		if(i==bat_cali_size-1)
			chg= 100;
		else if(i<0)
			chg= 0;//!!!!!!!!!
		else
		{
			//approx with line
			tmp = tmp - bat_cali[i];
			uint16_t range = bat_cali[i+1]-bat_cali[i];
			uint16_t partial = (tmp*20/range);
			chg =  (i*100/bat_cali_size)+partial;		
		}
		filter_bat.addInput(chg);
	}
	return filter_bat.getValue();
}

// Easter Egg
  
void still_alive()
{
	Serial1.write(217);
	
	Serial1.write(232);
	Serial1.write(211);  
	Serial1.write(230);
	Serial1.write(229);
	Serial1.write(227);
	Serial1.write(227);
  
	Serial1.write(229);
	Serial1.write(232);
	Serial1.write(213);
	Serial1.write(232);
	Serial1.write(212);
	Serial1.write(232);
  
	Serial1.write(232);
	Serial1.write(211);
	Serial1.write(232);
	Serial1.write(220);
	Serial1.write(230);
	Serial1.write(229);
	Serial1.write(227);
	Serial1.write(227);
  
	Serial1.write(212);
	Serial1.write(232);
	Serial1.write(211);
	Serial1.write(229);
	Serial1.write(225);
	Serial1.write(212);
	Serial1.write(232);
	Serial1.write(211);
	Serial1.write(227);
	Serial1.write(212);
	Serial1.write(220);
  
	Serial1.write(211);
	Serial1.write(232);
	Serial1.write(213);
	Serial1.write(232);
	Serial1.write(212);
	Serial1.write(232);
  
	Serial1.write(227);
	Serial1.write(211);
	Serial1.write(229);
	Serial1.write(212);
	Serial1.write(230);
	Serial1.write(211);
	Serial1.write(227);
	Serial1.write(212);
	Serial1.write(224);
  
	Serial1.write(211);
	Serial1.write(225);
	Serial1.write(227);
	Serial1.write(232);
	Serial1.write(220);
	Serial1.write(225);
	Serial1.write(227);
  
	Serial1.write(228);
	Serial1.write(227);
	Serial1.write(225);
	Serial1.write(223);
	Serial1.write(212);
	Serial1.write(232);
  
}

void still_alive_chorus()
{
	//Chorus//
	Serial1.write(217);
	Serial1.write(211);
	Serial1.write(220);
	Serial1.write(221);
  
	Serial1.write(212);
	Serial1.write(223);
	Serial1.write(228);
	Serial1.write(211);
	Serial1.write(227);
	Serial1.write(225);
	Serial1.write(225);
	Serial1.write(223);
  
	Serial1.write(225);
	Serial1.write(223);
	Serial1.write(212);
	Serial1.write(223);
	Serial1.write(210);
	Serial1.write(223);
	Serial1.write(232);
	Serial1.write(211);
	Serial1.write(232);
	Serial1.write(232);
	Serial1.write(220);
	Serial1.write(221);
  
	Serial1.write(212);
	Serial1.write(223);
	Serial1.write(229);
	Serial1.write(211);
	Serial1.write(230);
	Serial1.write(228);
	Serial1.write(227);
	Serial1.write(225);
  
	Serial1.write(225);
	Serial1.write(227);
	Serial1.write(212);
	Serial1.write(228);
	Serial1.write(210);
	Serial1.write(228);
	Serial1.write(232);
	Serial1.write(211);
	Serial1.write(232);
	Serial1.write(232);
	Serial1.write(230);
	Serial1.write(218);
	Serial1.write(220);
  
	Serial1.write(221);
	Serial1.write(221);
	Serial1.write(212);
	Serial1.write(220);
	Serial1.write(217);
	Serial1.write(212);
	Serial1.write(230);
	Serial1.write(211);
	Serial1.write(228);
	Serial1.write(230);
  
	Serial1.write(218);
	Serial1.write(220);
	Serial1.write(220);
	Serial1.write(217);
	Serial1.write(212);
	Serial1.write(230);
	Serial1.write(228);
	Serial1.write(211);
	Serial1.write(225);
	Serial1.write(223);
  
	Serial1.write(225);
	Serial1.write(228);
	Serial1.write(228);
	Serial1.write(212);
	Serial1.write(227);
	Serial1.write(211);
	Serial1.write(229);
	Serial1.write(212);
	Serial1.write(229);
  
	Serial1.write(232);
	Serial1.write(213);
	Serial1.write(232);
}